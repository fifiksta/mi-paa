require "./helpers.rb"

class BruteForceSolver

	def initialize(problem)
		@problem = problem
	end

	def solve(state=State.new(@problem), depth=0)
		return state if depth == @problem.n
		solve(state.withItem(depth), depth + 1)
			.select_better(solve(state, depth + 1))
	end

end


class BnBSolver

	def initialize(problem, guess_best_price = true)
		@problem = problem
		h = HeuristicSolver.new(@problem.clone) if guess_best_price
		@estimations = problem.c.reverse.cumsum.reverse
		@best_price = guess_best_price ? h.solve().c : 0
	end

	def solve(state=State.new(@problem), depth=0)
		return state if depth == @problem.n || state.too_heavy? || state.c+@estimations[depth] < @best_price
		@best_price = [state.c, @best_price].max
		solve(state.withItem(depth), depth + 1)
			.select_better(solve(state, depth + 1))
	end

end


class HeuristicSolver

	def initialize(problem)
		@problem = problem
		@problem.sort {|a,b| problem.c[a]/problem.w[a] <=> problem.c[b]/problem.w[b]}
	end

	def solve
		solution = real_solve
		return solution.alterState(@problem.unsort(solution.x))
	end

	def real_solve(state=State.new(@problem), depth=0)
		return state if depth == @problem.n
		state.withItem(depth).too_heavy? ? real_solve(state,depth + 1) : real_solve(state.withItem(depth), depth + 1)
	end
end


class DPSolver

	def initialize(problem)
		@table = Hash.new(nil)
		@problem = problem
		@cmax = @problem.c.reduce(:+)
		(0...@problem.n).each { |row|
			self[row,0]=0
		}
	end

	def solve
		(1...@cmax).each { |col|
			(1..@problem.n).each { |row|
				price, item = col, row-1
				col_without = price - @problem.c[item]
				weight = (col_without < 0) ? nil : self[row-1, col_without]
				weight += @problem.w[item] unless weight.nil?
				self[row,col] = better(self[row-1,col], weight) {|a,b| [a,b].min}
			}
		}
		reconstructSolution
	end

	def reconstructSolution
		row, col, solution = @problem.n, @cmax, State.new(@problem)
		col -= 1 while col >= 0 && (self[row,col].nil? || self[row,col] > @problem.m)
		
		(1..@problem.n).each do
			price, item = col, row-1
			if self[row,col] != self[row-1,col]
				solution.addItem(item);
				col = price - @problem.c[item]
			end
			row -= 1
		end
		return solution
	end

	def [](row,col)
		@table[row.to_s+":"+col.to_s]
	end

	def []=(row,col,val)
		@table[row.to_s+":"+col.to_s] = val
	end

	def better(a,b,&block)
		return a if b.nil?
		return b if a.nil?
		return block.call(a,b)
	end

	def to_s
		(0...@cmax).map { |col|
			([col]+(0..@problem.n).map { |row|
				self[row,col].nil? ? "." : self[row,col]
			}) .join("\t")
		} .join("\n")
	end

end

class FPTASSolver < DPSolver

	def initialize(problem, epsilon = 0.5)
		@original_problem, problem = problem, problem.clone
		b = epsilon > 0 ? (Math.log((epsilon*problem.c.reduce(:+))/problem.n)/Math.log(2)).floor : 0
		problem.c = problem.c.map { |x| x >> b } if b > 0
		super(problem)
	end

	def reconstructSolution
		super.alterProblem(@original_problem)
	end

end
