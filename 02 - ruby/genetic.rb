require_relative 'helpers.rb'
require_relative 'loader.rb'
require 'colorize'


class GeneticSolver

	attr_reader :population, :iteration_count
	attr_accessor :problem

	def initialize(problem)
		@problem = problem
		@prev_best = 0
		@best_break = 0
	end

	def solve(population_size=100, mutant_p = 0.25, hybrid_p = 0.25, random_p = 0.25, iterations = -1, best_break = 50, min_iterations = 0)
		@population = create_population(population_size)
		@best_break_max = best_break
		@min_iterations = min_iterations
		@best_break = 0
		@iteration_count = 0
		evolution(population_size,
			   (population_size * mutant_p).to_i,
			   (population_size * hybrid_p).to_i,
			   (population_size * random_p).to_i,
			   iterations)
		print "\r"+" "*160+"\r"
	end

	def evolution(population_size, mutant_count, hybrid_count, random_count, iterations)
		loop do
			if @population.first.c == @prev_best 
				@best_break += 1
				return @population if @best_break >= @best_break_max && @iteration_count >= @min_iterations
			else
				@prev_best = @population.first.c
				@best_break = 0
			end
			if iterations == 0
				return @population
			end
			@iteration_count += 1

			evolve(population_size, mutant_count, hybrid_count, random_count, iterations)
			solutions = (@population.select {|o| !o.too_heavy? }.length*1.0 / @population.length)
			sols = "%2.0f" % (solutions * 100)
			print "\r" + (" " * 150)
			rerror = @problem.known_solution.nil? ? "" : "|"+"#{format("%2.3f", (@problem.known_solution.c - @population.first.c)/(@problem.known_solution.c*1.0))} %".yellow
			print "\r"+"#{@population.first.x.join}".bold.cyan+"|iterace: #{@iteration_count}" + rerror + "|best: " + "#{@population.first.c}".bold.red +  "|solutions: #{sols}%|#{@population.first.w}|#{@problem.m}"
			iterations -= 1
			mutant_count, hybrid_count, random_count = yield [solutions, population_size, mutant_count, hybrid_count, random_count] if block_given?
		end
	end

	def evolve(population_size, mutant_count, hybrid_count, random_count, iterations)
		elite_count = population_size - (hybrid_count + mutant_count + random_count)
		hybrids = crossover(hybrid_count) { |a,b|
			x = Array.new(@problem.n) {|i| [a.x[i],b.x[i]].shuffle }
			[ State.new(@problem).alterState(x.map { |y| y[0]}),
			  State.new(@problem).alterState(x.map { |y| y[1]}) ]				
			# [State.new(@problem).alterState(Array.new(@problem.n) {|i|
			#  	[a,b].sample.x[i]
			# })]
		}
		mutants = mutation(@population.take(elite_count), mutant_count) { |x|
			i = (0...@problem.n).to_a.sample
			x = x.clone
			x[i] = 1 - x[i]
			x
		}
		@population = selection (elite_count) { |o| o.too_heavy? ? 1 - (1.0/o.c) : o.c }
		@population.concat create_population(random_count)
		@population.concat mutants
		@population.concat hybrids
		@population << State.new(@problem)
		@population.uniq!
	end

	def selection(size, &block)
		@population.sort_by!(&block).reverse!.take(size)
	end


	def crossover(count)
		hybrids = Array.new
		count.times do
			mates = @population.sample 2
			hybrids.concat yield(mates)
		end
		hybrids
	end

	def mutation(population, count)
		mutants = Array.new
		count.times do
			mutants << yield(@population.sample)
		end
		mutants
	end


	def create_population(size)
		Array.new(size) do 
			state = State.new(@problem)
			@problem.n.times do |i|
			 	state.addItem(i) if [true, false].sample
			end
			state
		end
	end

end

require "benchmark"

problems = Loader.new.loadTestingProblem 100
#problems = Loader.new.loadProblem('data/knap_100.dat')
terr = 0
avg_it = 0
avg_time = 0
n = 0
puts "cena \trel.chyba \titerace \tdoba".blue
problems.each do |problem|
		solver = GeneticSolver.new(problem)
		time = Benchmark.measure do
			solver.solve(500,		# velikost populace
						 0.15,		# % hybridu
						 0.25,		# % mutantu
						 0.05,		# % random
						 5000,		# max # iteraci
						 50,		# # iteraci se stejnym best
						 0 		# min # iteraci
						 ) { |solutions, population_size, mutant_count, hybrid_count, random_count|
				#[mutant_count, hybrid_count, random_count]
				[
					solutions*population_size,
					(1-solutions/2)*0.8*population_size,
					(1-solutions/2)*0.8*population_size
				]

			}
		end
		utime = "%0.5f" % (time.utime)

		solutions = solver.population.select { |x| x.w <= x.problem.m } .take(10).map {|x| x.c} 
		solution = solutions.first
		if !problem.known_solution.nil?
			error = (problem.known_solution.c - solution) * 1.0 / problem.known_solution.c
			terr += error
			erf = "#{format("%0.4f",(error * 100))} %"
			erf = (error != 0) ? erf.red : erf.green
		else
			erf = "---------".yellow
		end
	avg_it += solver.iteration_count
	n+=1
	avg_time += time.utime
	#puts "#{avg_time/n}s\t#{avg_it/n}it\t#{erf}"
	puts "#{solution} \t#{erf} \t#{solver.iteration_count} \t\t#{utime} s"
end
puts "AVG:\t#{format("%0.4f",terr/problems.length)} %"