class Problem

	attr_reader :id, :n, :m, :w, :c
	attr_writer :c
	attr_accessor :known_solution

	def initialize(id, n, m, w, c)
		@id, @n, @m, @w, @c = id, n, m, w, c
	end

	def clone
		return Problem.new(@id, @n, @m, Array.new(@w), Array.new(@c)) 
	end

	def sort(&block)
		@idxs = (0...@n).map {|x|x}
		@idxs.sort! &block
		@w = @idxs.map{|i| @w[i]}
		@c = @idxs.map{|i| @c[i]}
	end

	def unsort(array = nil)
		w = Array.new(@n,0)
		c = Array.new(@n,0)
		a = Array.new(@n,0) unless array == nil
		@idxs.each.with_index do |x,i|
			w[x], c[x] = @w[i], @c[i]
			a[x] = array[i] unless array == nil
		end
		@w, @c = w, c
		return a unless array == nil
	end

	def to_s
		"##{@id} (n=#{@n}, m=#{@m}):\nw#{w}\nc#{c}"
	end
end

class Solution

end

class Logger

	def initialize(name)
		@file = File.new("logs/#{name}.log", "w")
	end

	def log(str)
		@file.write("#{str}\n")
		@file.flush
	end

	def destroy
		@file.close
	end

end


class State

	attr_reader :x, :w, :c, :problem

	def initialize(problem)
		@problem = problem
		@x = Array.new(@problem.n, 0)
		@w, @c = 0, 0
		@dirty = true
	end

	def clone
		state = State.new(@problem)
		state.x = Array.new(@x)
		state.c, state.w = @c, @w
		state
	end

	def [](i)
		@x[i]
	end

	def withItem(i)
		clone.addItem(i)
	end

	def addItem(i)
		self.[]=(i,1)
	end

	def []=(i,value)
		@c += value*@problem.c[i] - x[i]*@problem.c[i]
		@w += value*@problem.w[i] - x[i]*@problem.w[i]
		@x[i] = value
		@dirty = true
		self
	end

	def to_s
		ok = too_heavy? ? "!!!" : ""
		"c:#{c} w:#{@w} #{ok}:\n#{x.to_s}"
	end

	def select_better(that)
		return that if too_heavy?
		return self if that.too_heavy?
		return self if that.w < @w
		return that 
	end

	def too_heavy?
		@w>@problem.m
	end

	def alterState(x)
		@x = x
		@w, @c = 0, 0 
		(0...@problem.n).each { |i|
			@w += x[i] * @problem.w[i]
			@c += x[i] * @problem.c[i]
		}
		self
	end

	def alterProblem!(problem)
		@problem = problem
		@c = @x.each_with_index.reduce(0) do |acc, elem| 
			val,idx = elem; 
			acc + problem.c[idx]*val
		end
		self
	end

	def alterProblem(problem)
		clone.alterProblem!(problem)
	end

	def hash
		if @dirty
			@hash = @x.join.to_i(2)
			@dirty = false
		end
		@hash
	end

	def eql?(other)
		other.hash == hash
	end

	protected

	attr_writer :x, :w, :c

end