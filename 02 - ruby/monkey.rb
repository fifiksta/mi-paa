class Array

	def select_with_index(&block)
		return to_enum(__method__) unless block_given?
		collect.with_index.select(&block).map &:first
	end

	def cumsum
		sum = 0
		map { |x| sum += x }
	end

end
