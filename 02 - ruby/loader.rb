require "./monkey.rb"

class Loader

	def loadTestingProblem(n)
		problems = loadProblem("data/knap_#{n.to_s}.inst.dat")
		loadSolution("data/knap_#{n.to_s}.sol.dat", problems)
		problems
	end

	def loadProblem(file)
		File.open(file).each_line.map do |line| 
			line = line.split(' ').map { |x| x.to_i }
			Problem.new(line.shift(),
						line.shift(),
						line.shift(),
						line.select_with_index { |x,i| i.even? },
						line.select_with_index { |x,i| i.odd?  }
			)
		end
	end

	def loadSolution(file, problems)
		File.open(file).each_line.with_index.map do |line, i|
			line = line.split(' ').map {|x| x.to_i }
			solution = State.new(problems[i])
			problems[i].known_solution = solution.alterState(line[3..-1])
		end
	end

end