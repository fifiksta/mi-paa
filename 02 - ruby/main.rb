#! /usr/bin/env ruby

require "./loader.rb"
require "./helpers.rb"
require "./algo.rb"
require "benchmark"

#config
solvers = [
#	HeuristicSolver, 
	DPSolver, 
#	BnBSolver, 
#	BruteForceSolver,
#	FPTASSolver,
]

problems = [
	4,
	10,
	15,
	20,
	22,
	25,
	27,
	30,
	32,
	35,
	37,
	40,
]

# repetition multiplier
MULT = 2

solvers.each do |solver|
	slogger = Logger.new("#{solver.name}")
#	problems.each do |n|
#		logger = Logger.new("#{solver.name}_#{n}")
		Loader.new.loadProblem('data/knap_100.dat').each do |problem|		
			repetions = 1.0/MULT
			solution = nil
			begin
				repetions = (repetions*MULT).to_i
				time = Benchmark.measure {
					repetions.times {
						solution = solver.new(problem).solve()
					}
				}
			end while time.utime == 0
			utime = "%0.10f" % (time.utime/repetions)
			log =  "#{problem.id};#{problem.n};#{problem.m};#{solution.c};#{utime}"
			puts log
			slogger.log(log)
			logger.log(log)
#		end
#		logger.destroy
	end
	slogger.destroy
end
