

class Population < Array

  attr_reader :genotype, :problem

  def initialize(problem, genotype)
    @genotype = genotype
    @problem = problem
  end

  def best_n(n = 1)

  end

  def selection(n)
    uniq!
    sort_by { |genotype| genotype.fitness }
    pop([0, size - n].max)
  end

  def crossover

  end

  def mutation
    gen = rand vector_size
    each { |genotype|
      genotype[gen]
    }
  end

  def noise(count)
    generated_noise = (0...count).map { @genotype.random(problem) }
    concat generated_noise
    generated_noise
  end

  def vector_size
    first.length
  end

  def with_fitness
    map { |genotype| [genotype, genotype.fitness]}
  end

end