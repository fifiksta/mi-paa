
class Genotype < Array

  attr_accessor :problem

  def crossover(other, method = :uniform)
    case method
      when :single_point
        single_point_crossover other
      when :two_points
        two_points_crossover other
      else
        uniform_crossover other
    end
  end

  def single_point_crossover(other)
    point = rand length
    mask = (0...length).map { |i| i < point }
    uniform_crossover(other, mask)
  end

  def two_points_crossover(other)
    a, b = (0...length).to_a.sample(2).sort
    mask = (0...length).map { |i| i < a || i > b }
    uniform_crossover(other, mask)
  end

  def uniform_crossover(other, mask = nil)
    if mask.nil?
      ran = [true, false]
      mask = map { ran.sample }
    end
    self.class.new zip(other).with_index.map {|x, i| mask[i] ? x.first : x.second }
  end

  def self.random(problem, values=[false, true])
    g = self.new((1..problem.variables).map { values.sample })
    g.problem = problem
    g
  end

  def mutate

  end

  def fitness
    raise NotImplementedError
  end

end
