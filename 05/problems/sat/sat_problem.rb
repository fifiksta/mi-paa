
class SatProblem

  attr_reader :variables
  attr_accessor :weights

  def initialize(formula, weights = nil)
    @formula = formula
    @variables = (formula.scan(/\d+/).reduce(0){|a,x| [a,x.to_i].max})
    if weights == nil
      @weights = Array.new(@variables, 1)
    end
  end

  def self.from_dimacs_cnf(str)
    # rozdelit na klauzule
    f = str.strip
           .split('0')
           .map { |c|
             c.strip
              .split(/\s+/)
              .join(' or ')
           }
           .join(') and (')
    # obalit zavorkama a pripravit pro interpolaci
    f = "\"( #{f} )\""
    f = f.gsub('-', 'not ') # doplnit negace
    # pripravit pro interpolaci
    f = f.gsub(/(\d+)/, '#{x[\1 - 1]}')
    SatProblem.new f
  end

  def value(x)
    evaluate(x).last
  end

  def evaluate(x)
    [
      eval(eval(@formula)),
      x.zip(weights).map {|x, w| (x ? 1 : 0) * w} .reduce(:+)
    ]
  end

  def to_s
    @formula
  end

end