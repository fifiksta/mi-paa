require_relative '../../libs/genotype'

class SatGenotype < Genotype

  def fitness
    ok, value = problem.evaluate(self)
    ok ? value : 1.0/(value+1)
  end

end