require 'rspec'
require_relative '../../genetic.rb'
require_relative '../../problems/sat/sat'

describe "MI-PAA ulohy" do

  before :all do
    pop_cap = 3

    @iterations = 100
    @step = Proc.new do |population, iteration|
      if population.size == 0
        population.noise(pop_cap)
      end
      population.selection pop_cap
      population.crossover
      population.mutation
      population.noise(5)
      population
    end
  end

  it '4 promene 6 klauzuli' do
    zadani = '1 -3 4 0 -1 2 -3 0 3 4 0 1 2 -3 -4 0 -2 3 0 -3 -4 0'
    problem = SatProblem.from_dimacs_cnf(zadani)
    gp = GeneticProgramming.new(problem, SatGenotype, &@step)
    gp.start(@iterations)
  end

  it '9 promene 6 klauzuli' do
    zadani = '1 2 3 0 -2 -3 -4 0 4 5 6 0 -6 -7 -8 0 7 8 9 0 -9 -1 0'
    problem = SatProblem.from_dimacs_cnf(zadani)
    gp = GeneticProgramming.new(problem, SatGenotype, &@step)
    gp.start(@iterations)
  end


end