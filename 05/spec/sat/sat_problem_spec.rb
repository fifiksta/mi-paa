require 'rspec'
require_relative '../../problems/sat/sat_problem'

describe SatProblem do

  before :each do
    @dimacs = '1 -3 4 0 -1 2 -3 0 3 4 0 1 2 -3 -4 0 -2 3 0 -3 -4 0'
  end

  it 'read dimacs cnf input' do
    problem = SatProblem.from_dimacs_cnf(@dimacs)
    puts problem
    expect(problem.variables).to eq 4
  end

  it 'should assign variables' do
    problem = SatProblem.from_dimacs_cnf(@dimacs)
    expect(problem.evaluate([true, true, true, false]).first).to be true
  end

  it 'should calculate weight for instance' do

  end




end

