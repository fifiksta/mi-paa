require 'rspec'
require_relative '../../libs/population'
require_relative '../../libs/genotype'

describe Population do

  it 'should be clonable' do
    genotype = Genotype
    problem = Class.new
    population = Population.new(problem, genotype)
    cloned = population.clone
    expect(cloned).to be_a Population
    expect(cloned.problem).to be problem
    expect(cloned.genotype).to be genotype
  end
end