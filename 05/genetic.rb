require_relative 'libs/genotype'
require_relative 'libs/population'

class GeneticProgramming

  def initialize(problem, genotype, &block)
    @current_population = Population.new(problem, genotype)
    @step = block
  end

  def start(iterations = 100)
    (0..iterations).each do |iteration|
      new_generation = @step.call(@current_population.clone, iteration)
      @current_population = new_generation
    end
  end

end