#!/usr/bin/env node
 
var fs = require('fs');
var backpack = require('./backpack');
var solutions = {};

var init = function(){
    [4,10,15,20,22,25,27,30,32,35,37,40].forEach(function(n){
        solveWithSolvers(n, [backpack.Solvers.BruteForce, backpack.Solvers.Heurestic]);
    });
};

var solveWithSolvers = function(n, solvers){
    var input = loadProblem(fs.readFileSync("data/knap_"+n+".inst.dat", 'utf-8'));
    fs.appendFileSync("out/"+n+".out","id;n;solver;time;N;solution\n");
    input.map(function(problem){
        var solution = getSolution(problem.n, problem.id);
        var times = solvers.map(function(solver){
            var t = process.hrtime();

            var solved = solver(problem);
            
            t = process.hrtime(t);
            solved.time = t;

            var output = [problem.id,problem.n,solver.Name,t,solved.N,solved.solution.join("")].join(";");
            fs.appendFileSync("out/"+n+".out",output+"\n");
            console.log(output);
            
            if (arrayEquals(solved.solution, solution.x)){
                //console.log(solver.name+":", problem.id, "OK", solved.timeString(), "N:", solved.N);
                return t;
            } else if (solved.price === solution.v){
                //console.error(solver.name+":", problem.id, "Different Solution", "Got:", solved.solution, "("+solved.price+")", "Was:", solution.x, "("+solution.v+")");
                return t;
            }
            //console.error(solver.name+":", problem.id, "Wrong Solution", "Got:", solved.solution, "("+solved.price+")", "should be:", solution.x, "("+solution.v+")");
            return Math.Infinity;
        });
    });
};


/**
 * Finds solution in .sol.dat
 */
var getSolution = function(n, id){
    if (solutions[n] === undefined)
        solutions[n] = loadSolution(fs.readFileSync("data/knap_"+n+".sol.dat",  'utf-8'));
    for (var i in solutions[n])
        if (solutions[n][i].id === id)
            return solutions[n][i];
};
 
var loadSolution = function(input){
    var solutions = input.split('\n').map(function(x){
        var y = x.split(" ").map(function(x){
            return parseInt(x,10);
        });
        return {
                id :y.shift(),
                n: y.shift(),
                v: y.shift(),
                x: y.shift() || y,
            };
    });
    solutions.pop();
    return solutions;
};


var loadProblem = function(input){
    var problems = input.split('\n').map(function(x){
        var y = x.split(" ").map(function(x){
            return parseInt(x,10);
        });
        return {
                id :y.shift(),
                n: y.shift(),
                M: y.shift(),
                w: y.filter(function(x,i){
                    return (i%2) === 0;
                }),
                c: y.filter(function(x,i){
                    return (i%2) === 1;
                })
            };
    });
    problems.pop();
    return problems;
};


var arrayEquals = function(a,b) {
  for ( var i in a) {
    if (a[i] !== b[i]) return false;
  }
  return true;
};


 
init();
