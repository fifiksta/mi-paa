var BruteForceSolver = function(problem){
    var N = 0;
    var solver = function(problem, solution, index){
        N += 1;
        if (solution.weight > problem.M)
            return null;
        if (index === problem.n){
            return solution;
        }
     
        var solutionWith    = solver(problem, solution.clone().setBit(index,1), index + 1);
        var solutionWithout = solver(problem, solution.clone()                , index + 1);
        return Solution.better(solutionWith, solutionWithout);
    };
    var solution = solver(problem, new Solution(problem), 0);
    solution.N = N;
    return solution;
};
BruteForceSolver.Name = "Brute";




var HeuresticSolver = function(problem){
    var N = 0;
    problem = {
        id: problem.id,
        n: problem.n,
        M: problem.M,
        w: problem.w,
        c: problem.c,
        indexes : problem.w.map(function(x,i){
            return i;
        })
    };
    // create maping arrays for forward and reverse reordering
    problem.indexes.sort(function(a,b){ return problem.c[a]/problem.w[a] - problem.c[b]/problem.w[b]; });
    problem.indexes_rev = problem.indexes.slice(0);
    problem.indexes.forEach(function(x,i){ problem.indexes_rev[x] = i; });

    // reorder weigth and price arrays
    problem.w = problem.indexes.map(function(x,i){ return problem.w[x]; });
    problem.c = problem.indexes.map(function(x,i){ return problem.c[x]; });

    // calculate right cumulative sum
    var cumsum = 0;
    problem.cumsum = problem.c.slice(0).reverse().reduce(function(lst, x){cumsum += x; lst.push(cumsum); return lst;}, []).reverse();
    
    problem.best = -Infinity;

    var solver = function(problem, solution, index){
        N +=1;
        if (solution.weight > problem.M)
            return null;

        if (index === problem.n){
            problem.best = Math.max(solution.price, problem.best);
            return solution;
        }

        if (solution.price  + problem.cumsum[index] < problem.best){
            return null;
        }
     
        var solutionWith = solver(problem, solution.clone().setBit(index,1), index + 1);
        var solutionWithout = solver(problem, solution.clone(),              index + 1);

        return Solution.better(solutionWith, solutionWithout);
    };

    // reorder solution to original order
    var solution = solver(problem, new Solution(problem), 0);
    //console.log(solution.solution, problem.indexes_rev.map(function(x,i){ return solution.solution[x]; }));
    solution.solution = problem.indexes_rev.map(function(x,i){ return solution.solution[x]; });
    solution.N = N;
    return solution;
};
HeuresticSolver.Name = "Heuristic";





var Solution = function(problem, solution, weight, price){
    if (typeof solution === "undefined")
        solution = problem.n;
    if (typeof solution === "number")
        solution = Array.apply(null, new Array(solution)).map(Number.prototype.valueOf,0);
    this.problem = problem;
    this.solution = solution;
    this.weight = weight || 0;
    this.price = price || 0;
};
 
Solution.prototype = {
    clone : function(){
        return new Solution(this.problem, this.solution.slice(0), this.weight, this.price);
    },
    setBit : function(index, value){
        var old = this.solution[index];
        this.solution[index] = value;
        this.weight += -1*old*this.problem.w[index] + value*this.problem.w[index];
        this.price  += -1*old*this.problem.c[index] + value*this.problem.c[index];
        return this;
    },
    toString : function(){
        return this.problem.id+" "+this.problem.n+" "+this.problem.M+" c:"+this.price+" w:"+this.weight+" "+this.solution.join(" ")+this.timeString();
    },
    timeString : function(){
        return " (took "+(this.time[0]+this.time[1]*Math.pow(10,-9))+" s)";
    }
};
 
Solution.better = function(that, other){
    if (that === null)
        return other;
    if (other === null)
        return that;
    return that.price > other.price?that:other;
};

module.exports = {
    Solvers  : {
        BruteForce : BruteForceSolver,
        Heurestic  : HeuresticSolver
    }
};